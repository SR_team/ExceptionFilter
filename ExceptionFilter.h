#pragma once

struct _EXCEPTION_POINTERS;

class ExceptionFilter {
	void *entryPtr_;
	int entrySize_;

	void *handler_;

public:
	ExceptionFilter() noexcept;
	virtual ~ExceptionFilter();

protected:
	virtual long filter( _EXCEPTION_POINTERS *ep ) noexcept = 0;

private:
	long rawCaller( _EXCEPTION_POINTERS *ep ) noexcept;

	void allocateMemForEntryCode() noexcept;
	void makeEntryFunction() noexcept;
};
