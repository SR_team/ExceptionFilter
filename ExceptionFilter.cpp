#include "ExceptionFilter.h"
#include <cstring>
#if !defined(_WIN64) && (__has_include( <llmo/SRAllocator.h>) || __has_include(<SRAllocator.h>) || __has_include( "llmo/SRAllocator.h") || __has_include("SRAllocator.h")) && (__has_include( <llmo/fn2void.hpp>) || __has_include(<fn2void.hpp>) || __has_include( "llmo/fn2void.hpp") || __has_include("fn2void.hpp"))
#	if __has_include( <llmo/SRAllocator.h>)
#		include <llmo/SRAllocator.h>
#	elif __has_include( <SRAllocator.h>)
#		include <SRAllocator.h>
#	elif __has_include( "llmo/SRAllocator.h" )
#		include "llmo/SRAllocator.h"
#	elif __has_include( "SRAllocator.h" )
#		include "SRAllocator.h"
#	endif
#	if __has_include( <llmo/fn2void.hpp>)
#		include <llmo/fn2void.hpp>
#	elif __has_include( <fn2void.hpp>)
#		include <fn2void.hpp>
#	elif __has_include( "llmo/fn2void.hpp" )
#		include "llmo/fn2void.hpp"
#	elif __has_include( "fn2void.hpp" )
#		include "fn2void.hpp"
#	endif
#	include <errhandlingapi.h>
#	define kAvailableLLMO
#else
#	include <cstdint>
#	include <windows.h>
#endif

namespace {
#ifdef _WIN64
	static constexpr char kEntryUEF[] = "\x48\xb9\x00\x00\x00\x00\x00\x00\x00\x00\x48\xb8\x00\x00\x00\x00\x00\x00\x00\x00\xFF\xE0"; // mov rcx, this; mov rax, rawCaller; jmp rax
#else
	static constexpr char kEntryUEF[] = "\xb9\x00\x00\x00\x00\xE9\x00\x00\x00\x00"; // mov ecx, this; jmp rawCaller
#endif
}

ExceptionFilter::ExceptionFilter() noexcept {
	allocateMemForEntryCode();
	makeEntryFunction();
	handler_ = AddVectoredExceptionHandler( 1, PVECTORED_EXCEPTION_HANDLER( entryPtr_ ) );
}

ExceptionFilter::~ExceptionFilter() {
	RemoveVectoredExceptionHandler( handler_ );
#ifdef kAvailableLLMO
	SRHook::ptr_t ptr( (uint8_t *)entryPtr_, entrySize_ );
	SRHook::Allocator().deallocate( ptr );
#else
	VirtualFree( entryPtr_, entrySize_, 0 );
#endif
}

long ExceptionFilter::rawCaller( _EXCEPTION_POINTERS *ep ) noexcept {
	return filter( ep );
}

void ExceptionFilter::allocateMemForEntryCode() noexcept {
#ifdef kAvailableLLMO
	auto ptr = SRHook::Allocator().allocate( sizeof( kEntryUEF ) );
	entryPtr_ = ptr.ptr;
	entrySize_ = ptr.size;
#else
	SYSTEM_INFO sysInfo;
	GetSystemInfo( &sysInfo );
	entrySize_ = sysInfo.dwPageSize;
	entryPtr_ = VirtualAlloc( nullptr, sysInfo.dwPageSize, MEM_RESERVE | MEM_COMMIT, PAGE_EXECUTE_READWRITE );
	size_t address = (size_t)entryPtr_;
	size_t size = (size_t)sysInfo.dwPageSize;
	do {
		MEMORY_BASIC_INFORMATION mbi;
		if ( !::VirtualQuery( reinterpret_cast<PVOID>( address ), &mbi, sizeof( mbi ) ) ) break;
		if ( size > mbi.RegionSize )
			size -= mbi.RegionSize;
		else
			size = 0;
		DWORD oldp;
		if ( !::VirtualProtect( mbi.BaseAddress, mbi.RegionSize, PAGE_EXECUTE_READWRITE, &oldp ) ) break;
		if ( reinterpret_cast<size_t>( mbi.BaseAddress ) + mbi.RegionSize < address + size )
			address = reinterpret_cast<size_t>( mbi.BaseAddress ) + mbi.RegionSize;
	} while ( size );
	std::memset( entryPtr_, 0xCC, sysInfo.dwPageSize );
#endif
}

void ExceptionFilter::makeEntryFunction() noexcept {
	auto entryCode = (uint8_t *)entryPtr_;
	std::memcpy( entryCode, kEntryUEF, sizeof( kEntryUEF ) - 1 );
#ifdef _WIN64
	*(ExceptionFilter **)&entryCode[2] = this;
#else
	*(ExceptionFilter **)&entryCode[1] = this;
#endif
#ifdef kAvailableLLMO
	auto rawAddr = (size_t)fn2void( &ExceptionFilter::rawCaller );
#else
	struct f {
		decltype( &ExceptionFilter::rawCaller ) fn;
	} _{ &ExceptionFilter::rawCaller };
	auto rawAddr = *reinterpret_cast<size_t *>( &_ );
#endif
#ifdef _WIN64
	*(size_t *)&entryCode[12] = rawAddr;
#else
	auto relAddr = rawAddr - ( (size_t)&entryCode[5] + 5 );
	*(size_t *)&entryCode[6] = relAddr;
#endif
}

#ifdef kAvailableLLMO
#	undef kAvailableLLMO
#endif
