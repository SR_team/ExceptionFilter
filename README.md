# ExceptionFilter

The wrapper for `AddVectoredExceptionHandler` and `RemoveVectoredExceptionHandler`, with pure wirtual method `filter` to override this in you own exception filters.

## Usage

```cpp
class MyExceptionFilter : public ExceptionFilter {
protected:
	long filter( _EXCEPTION_POINTERS *ep ) noexcept override{
        // handle exception
        return 0; // EXCEPTION_CONTINUE_SEARCH
    }
};
```

## Requires

- C++11
- [LLMO](https://gitlab.com/SR_team/llmo) (optional for Win32)
